from flask import Flask
from flask_migrate import Migrate


def init_app(app: Flask):
    migrate = Migrate(compare_type=True)
    from app.models.log_model import LogArchives
    from app.models.user_model import User
    migrate.init_app(app, app.db)