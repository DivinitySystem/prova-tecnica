from dataclasses import dataclass
from sqlalchemy.orm import validates
from app.configs.database import db
from werkzeug.security import generate_password_hash,check_password_hash
import re


@dataclass
class User(db.Model):
    id:str
    name:str
    email:str

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40), nullable=False)
    email = db.Column(db.String(100), nullable=False, unique=True)
    password_hash = db.Column(db.String(200), nullable=False)    

    valid_keys = ['name', 'email', 'password']
    
    __tablename__ = 'users'

    @validates('email')
    def validate_email(self,_,email):   
        email_regex = r"^[\w-]+@[a-z\d]+\.[\w]{3}(.br)?"

        if not re.fullmatch(email_regex,email):
            raise ValueError("Invalid email, format should be equal to xxxx@xxxx.xxx.(xx)")
        return email    
    
    @validates('name')
    def validate_name(self,_, name):
        if type(name) != str or len(name) < 4:
            raise ValueError("Name should have length > 4 and be a string")
        return name
    
    @property
    def password(self):
        raise ValueError('password is not a readable attribute')
    
    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)
    
    
    def check_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)
