from dataclasses import dataclass
from app.configs.database import db

@dataclass
class LogArchives(db.Model):
    id:int
    date:str
    ip:str
    description:str
    
    __tablename__ = 'log_archives'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.String(15), nullable=False)
    ip = db.Column(db.String(20), nullable=False)
    description = db.Column(db.Text(), nullable=False)
    