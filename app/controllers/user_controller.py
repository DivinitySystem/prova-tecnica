from flask import request, jsonify
from werkzeug.exceptions import NotFound
from app.configs.database import db
from app.models.user_model import User
from sqlalchemy.exc import IntegrityError

from app import controllers

def post_user():
    data = request.json
    
    try:
        
        new_data = controllers.key_validator(User.valid_keys, data)
        password_to_hash = new_data.pop('password')
        
        new_user = User(**data)
        new_user.password = password_to_hash

        db.session.add(new_user)
        db.session.commit()
        response = jsonify(new_user)
        response.headers.add('Access-Control-Allow-Origin', '*')    
        
        return response,201


    except IntegrityError as e:
     
        return {"error": "Email already taken"},409

    except KeyError as e:
        return {
            "msg": f"Missing or Invalid data, {e.args[0]}",
            "Valid data": f"{User.valid_keys}",  
        },400

    except ValueError as e:
        return {"error": e.args[0]},400
    
def delete_user(id):
    try:
        query = User.query.filter_by(id=id).first_or_404()
        db.session.delete(query)
        db.session.commit()
        return "",204
    except NotFound:
        return {"error": "User Not Found"},404


def patch_user(id):
    try:
        user =  User.query.filter_by(id=id).first_or_404()
        data = request.json
        valid_data = controllers.patch_itens(User.valid_keys, data)
        
        if 'password' in valid_data: valid_data.pop('password')
       
        for key, value in valid_data.items():
            setattr(user,key,value)
        
        db.session.commit()
        return valid_data,202

    except KeyError as e:
         return {
            "msg": f"Missing or Invalid data, {e.args[0]}",
        },400

    except NotFound:
        return {"error": "User not found"}, 404

    except ValueError as e:
        return {"error": e.args[0]},400
    
    except IntegrityError as e:
        return {"error": "Email already taken"},409
