from flask import jsonify, request
from app.models.log_model import LogArchives
from app import controllers


def get_by_date():
    controllers.put_data_in_db()
    try:
        params = request.args
        params_dict = {}
        for i in params:
            params_dict[i] = params[i]
        description = params_dict['msg']
        date = ''
        if 'date' in params_dict: date = params_dict['date']
        if len(description) < 4:
            raise ValueError
        query = LogArchives.query.filter(LogArchives.description.like(f"%{description}%")).filter(LogArchives.date.like(f'%{date}%')).all()
        return jsonify(query)

    except KeyError:
        return {"error": "Must have msg arg"},400
    except ValueError:
        return {"error": "Arg msg too short"},400
    