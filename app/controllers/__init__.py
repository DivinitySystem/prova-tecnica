import os
from dotenv import load_dotenv
from app.models.log_model import LogArchives
from werkzeug.exceptions import NotFound
from app.configs.database import db
import ipdb

load_dotenv()

def read_authlog():
    """
    Read the auth.log and then returns a tuple of lists containing strings in the following order:

    date_list, ip_list, description_list
    """
    infile = os.environ.get('LOG_PATH')
    description_list = []
    ip_list = []
    date_list = []
    
    with open(infile) as f:
        log = f.readlines()
    for line in log:
        date = line.split(' ')[0:3]
        date_list.append('-'.join(date))
        ip_list.append(line.split(' ')[3])
           
        
        description = line.split(' ')[4::]
        description_list.append(' '.join(description))
    
    return (date_list, ip_list, description_list)



def key_validator(valid_keys, data):
    """

        Returns a dict with the data if the keys are in valid keys
        
        Else raises a KeyError with the invalid data

    """
    new_data = {}
    invalid_data = []
    for key in valid_keys:
        if key not in data:
            invalid_data.append(key)
        else:
            new_data[key] = data[key]

    if len(invalid_data) > 0:
        raise KeyError(f"{invalid_data}")
    
    return new_data


def patch_itens(keys, data):
    valid_keys = {}
    for key, value in data.items(): 
        if key in keys:
            valid_keys[key] = value
    
    if len(valid_keys) == 0: raise KeyError
    return valid_keys


def put_data_in_db():
    try:
        archives = LogArchives.query.first()
        if archives == None:
            raise NotFound
    except NotFound:
        
        date_list, ip_list, description_list = read_authlog()
        for idx, _ in enumerate(date_list):
            data = {"date": date_list[idx], 
            "ip":ip_list[idx], 
            "description": description_list[idx]
            }
            
            archive = LogArchives(**data)
            db.session.add(archive)
        
            
        db.session.commit()


