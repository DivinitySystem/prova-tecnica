from flask import request, jsonify
from app.exc.exc import InvalidPasswordError
from app.models.user_model import User
from werkzeug.exceptions import NotFound

def login():   
    try:
        email = request.json['email']
        password = request.json['password']
        user = User.query.filter_by(email=email).first_or_404()
        if not user.check_password(password):
            raise InvalidPasswordError
        response = jsonify(user)
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response,200
        
    except InvalidPasswordError:
        return {"error": "Invalid password"},401

    except NotFound:
        return {"error": "User not found"},404
    
    except KeyError:
        return {"error": "Missing key, email and password required"},400