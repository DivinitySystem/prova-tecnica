from flask import Flask
from app.routes.log_route import date_bp
from app.routes.user_routes import user_bp
from app.routes.login_route import login_bp

def init_app(app:Flask):
    app.register_blueprint(date_bp)
    app.register_blueprint(user_bp)
    app.register_blueprint(login_bp)

