from flask import Blueprint
from app.controllers.log_controller import get_by_date

date_bp = Blueprint("date_bp", __name__, url_prefix='/auth')

date_bp.get("")(get_by_date)
