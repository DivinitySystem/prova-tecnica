from requests import  post, delete

def test_login_route():
    user_url = 'http://127.0.0.1:5000/user'
    login_url = "http://127.0.0.1:5000/login"
    
    myobj = {'name': 'somevalue', 'password': 'pass', 'email': 'teste@gmail.com'}
    user_created = post(user_url, json=myobj).json()
    
    user_login = {"email": "teste@gmail.com", "password": "pass"} 
    invalid_user_login = {"email": "teste@gmail.com", "password": "pasaaass"} 

    user_post_invalid_password = post(login_url, json=invalid_user_login)


    assert user_post_invalid_password.status_code == 401
    assert user_post_invalid_password.json()['error'] == "Invalid password"  

    user_post = post(login_url,json=user_login)
    

    delete(f"http://127.0.0.1:5000/user/{user_created['id']}")


    assert user_post.status_code == 200
    assert user_post.json()['name'] == user_created['name']
    assert user_post.json()['email'] == user_created['email']
    assert 'id' in user_post.json()

    email_not_foud_data = {"email": "notfound@gmail.com", "password": "pass"} 
    user_post_email_not_found = post(login_url, json=email_not_foud_data)

    assert user_post_email_not_found.status_code == 404
    assert user_post_email_not_found.json()['error'] == "User not found"
    
    missing_keys = {}
    login_missing_keys = post(login_url, json=missing_keys)

    assert login_missing_keys.status_code == 400
    assert login_missing_keys.json()['error'] == "Missing key, email and password required"

