from requests import  post, patch, delete


def test_post_data():
    url = 'http://127.0.0.1:5000/user'
    myobj = {'name': 'somevalue', 'password': 'pass', 'email': 'teste@gmail.com'}
    data = post(url,json=myobj)
    id = data.json()['id']
    def test_valids_responses(**data):
        valid_response = ['id','name', 'password', 'email']
        for key in data:
            if key not in valid_response:
                return False
            if key != 'id':
                if myobj[key] != data[key]:
                    return False

        return True

    assert test_valids_responses(**data.json())
    assert data.status_code == 201
    assert type(data.json()) == dict


    url = f'http://127.0.0.1:5000/user/{id}'
    delete_data = delete(url)

    assert delete_data.status_code == 204
    

def test_wrong_data_on_post():
    url = 'http://127.0.0.1:5000/user'
    invalid_email = {"name": "somevalue", "password": "pass", "email": "testegmail.com"}
    data = post(url,json=invalid_email)
    
    assert data.status_code == 400
    assert data.json()['error'] == "Invalid email, format should be equal to xxxx@xxxx.xxx.(xx)"
    assert type(data.json()) == dict

    url = 'http://127.0.0.1:5000/user'
    invalid_name = {"name": "aa", "password": "pass", "email": "teste@gmail.com"}
    data = post(url,json=invalid_name)
    
    assert data.status_code == 400
    assert data.json()['error'] == "Name should have length > 4 and be a string"
    assert type(data.json()) == dict

    missing_keys = {}
    data = post(url, json=missing_keys)

    assert data.status_code == 400
    assert data.json()["msg"] ==  "Missing or Invalid data, ['name', 'email', 'password']"
    assert data.json()["Valid data"] == "['name', 'email', 'password']"

    valid_obj = {'name': 'somevalue', 'password': 'pass', 'email': 'teste@gmail.com'}
    data = post(url, json=valid_obj)
    id = data.json()['id']
    data_error = post(url, json=valid_obj)

    assert data_error.status_code == 409
    assert data_error.json()['error'] == "Email already taken"
    assert type(data_error.json()) == dict
    
    url = f'http://127.0.0.1:5000/user/{id}'

    delete(url)
     

def test_delete_not_found_user_route():
    url = 'http://127.0.0.1:5000/user/0'
    delete_data = delete(url)

    assert delete_data.status_code == 404
    assert delete_data.json()['error'] == "User Not Found"

def test_patch_user_route():
    url = 'http://127.0.0.1:5000/user'
    myobj = {'name': 'somevalue', 'password': 'pass', 'email': 'teste@gmail.com'}
    data = post(url, json=myobj)
    
    id = data.json()['id']
    url = f'http://127.0.0.1:5000/user/{id}'
    
    patch_obj= {'name': 'value1234', 'email': 'testasndo@gmail.com'}
    response = patch(url, json=patch_obj)

    delete(url)
    assert response.status_code == 202
    assert response.json()['name'] == patch_obj['name']
    assert response.json()['email'] == patch_obj['email']
