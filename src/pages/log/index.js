import axios from "axios";
import { useState } from "react";
import { Link } from "react-router-dom";
import { Button, Section, Error } from "./style";

const Log = () => {
  const [res, setRes] = useState([]);
  const [filterMsg, setFilterMsg] = useState("");
  const [error, setError] = useState(false);
  const [date, setDate] = useState("");
  const [notFound, setNotFound] = useState(false);

  const handleClick = (filterMsg, date) => {
    axios
      .get(`http://127.0.0.1:5000/auth?msg=${filterMsg}&date=${date}`)
      .then((res) => {
        setRes(res.data);
        setError(false);
        if (!res.data[0]) {
          setNotFound(true);
        } else {
          notFound(false);
        }
      })
      .catch((e) => setError(true));
  };
  return (
    <Section>
      <div>
        <input
          placeholder="Filtrar por mensagem"
          type="text"
          onChange={(e) => setFilterMsg(e.target.value)}
        ></input>
        <input
          placeholder="filtrar por data"
          type="text"
          onChange={(e) => setDate(e.target.value)}
        ></input>
        <span>Formato de data = Nov-30-12:17:01</span>
        <Button onClick={() => handleClick(filterMsg, date)} type="button">
          {" "}
          Clique aqui para procurar
        </Button>
        <p>
          Para voltar para o inicio clique <Link to="/">aqui</Link>
        </p>
        {error && <Error>Filtre por alguma mensagem</Error>}
      </div>
      <ul>
        {res[0] ? (
          <>
            {res.map((value) => (
              <li key={value.id}>
                <div>
                  <span>Data: {value.date}</span>
                  <span>
                    {value.ip} {value.description}
                  </span>
                </div>
              </li>
            ))}
          </>
        ) : (
          <>{notFound && <Error>Log não encontrado</Error>}</>
        )}
      </ul>
    </Section>
  );
};

export default Log;
