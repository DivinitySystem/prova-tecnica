import styled from "styled-components";

export const Section = styled.section`
  div {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  input {
    width: 300px;
    margin: 15px;
    margin-top: 20px;
  }
  ul {
    display: flex;
    flex-direction: column;
    align-items: center;
    li {
      list-style-type: none;
      align-items: center;
      justify-content: center;
      div {
        margin: 20px;
        border: solid 1px black;
      }
    }
  }
`;

export const Button = styled.button`
  width: 300px;
  font-size: 10px;
  border-radius: 10px;
  height: 40px;
  background-color: blue;
  color: white;
  :hover {
    opacity: 0.7;
  }
`;

export const Error = styled.p`
  font-size: 12px;
  color: red;
`;
