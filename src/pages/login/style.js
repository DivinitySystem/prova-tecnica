import styled from "styled-components";

export const Form = styled.form`
  display: flex;
  align-items: center;
  flex-direction: column;
  input {
    margin: 4px;
    width: 300px;
    height: 25px;
  }
  p {
    font-size: 12px;
    color: red;
  }
`;

export const Button = styled.button`
  width: 300px;
  font-size: 10px;
  border-radius: 10px;
  height: 40px;
  background-color: blue;
  color: white;
  :hover {
    opacity: 0.7;
  }
`;

export const Section = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
