import { useState } from "react";
import axios from "axios";
import { useNavigate, Link } from "react-router-dom";
import { Section, Button, Form } from "./style";

const Login = () => {
  const [password, setPassowrd] = useState("");
  const [email, setEmail] = useState("");
  const [error, setError] = useState(false);
  const navigate = useNavigate();

  const handleSubmit = (name, email) => {
    if (name && password && email) {
      const data = {
        password: password,
        email: email,
      };
      console.log(data);

      axios
        .post("http://127.0.0.1:5000/login", data)
        .then((res) => {
          localStorage.setItem("name", res.data.name);
          console.log(res.data);
          navigate("/auth");
        })
        .catch((e) => setError(true));
    }
  };

  return (
    <Section>
      <Form>
        <h1>Login</h1>
        <input
          placeholder="email"
          type="text"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        ></input>
        <input
          placeholder="password"
          type="password"
          value={password}
          onChange={(e) => setPassowrd(e.target.value)}
        ></input>
        {error && <p>Usuário ou senha inválidos</p>}
      </Form>
      <Button type="button" onClick={() => handleSubmit(password, email)}>
        Login
      </Button>
      <span>
        Não tem conta? Faça registro <Link to="/">aqui</Link>
      </span>
    </Section>
  );
};

export default Login;
