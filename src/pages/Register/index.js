import { useState } from "react";
import { Form, Section, Button } from "./style";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";

const Register = () => {
  const [name, setName] = useState("");
  const [password, setPassowrd] = useState("");
  const [email, setEmail] = useState("");
  const navigate = useNavigate();

  const handleSubmit = (name, password, email) => {
    if (name && password && email) {
      const data = {
        name: name,
        password: password,
        email: email,
      };

      axios
        .post("http://127.0.0.1:5000/user", data)
        .then((res) => {
          localStorage.setItem("name", res.data.name);
          console.log(res);
          navigate("/auth");
        })
        .catch((e) => console.log(e.response));
    }
  };

  return (
    <Section>
      <Form>
        <h1>Register</h1>
        <input
          placeholder="name"
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
        ></input>
        <input
          placeholder="password"
          type="password"
          value={password}
          onChange={(e) => setPassowrd(e.target.value)}
        ></input>
        <input
          placeholder="email"
          type="text"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        ></input>
      </Form>
      <Button onClick={() => handleSubmit(name, password, email)}>
        Registre-se aqui
      </Button>
      <span>
        Já tem conta? Faça login <Link to="/login">aqui</Link>
      </span>
    </Section>
  );
};

export default Register;
