import { Route, Routes } from "react-router-dom";
import Log from "../pages/log";
import Login from "../pages/login";
import Register from "../pages/Register";

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/login" element={<Login />}></Route>
      <Route path="/" element={<Register />}></Route>
      <Route path="/auth" element={<Log />}></Route>
    </Routes>
  );
};

export default AppRoutes;
